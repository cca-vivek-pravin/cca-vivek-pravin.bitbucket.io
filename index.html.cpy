<!DOCTYPE html>
<html lang="en">


<head>
	
    <meta charset="utf-8">
    <link rel="stylesheet" href =http://udayton-cloud.bitbucket.io/style1.css>
    <title>CCA-Test page of Vivek Manikandan</title>
    <script src="https://udayton-cloud.bitbucket.io/clock.js"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" crossorigin="anonymous"></script>
    <style>
    
    .owner{
        background-color: #4CAF50;
        font-family:calibri;
        color: white;
        padding: 5px;
        text-align: center;
        text-decoration: none;
        font-size: 12px;
        margin: 4px 2px;
        cursor: pointer;

    }

    .button{
        background-color: #4CAF50;
        border: none;
        color: white;
        font-family:calibri;
        padding: 5px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 12px;
        margin: 4px 2px;
        cursor: pointer;
    }
    h2{color:white;
   text-align:center;
   font-family:calibri;}

    h3{text-align:center;
   font-family:calibri;}

    p{text-align:center;
  font-family:calibri;
  font-size: 18px;}
    .round{border-radius: 8px;}
    .response{background-color: #ff9800;}

</style>
</head>
<body>

<div class="container wrapper">
    <div id="top">
    <div class= "owner">
            <h2><b><u>CPS 592 CLOUD COMPUTING AND APPLICATION</u><b></h2>
            <h2><b>By Phu Phung</b></h2>
            <h1><b>by Vivek Manikandan and Arun Pravin</b></h1>
    </div>
            <!--div id = "email" onclick="showhideemail()">Show my email</div>
            <script src = "email.js"></script-->
        
    </div>
    <div class="wrapper">
        <div id="menubar">
            <canvas id ="analog-clock" width = "150" height = "150" style="background-color:#999"></canvas>
            </div>
        </div>
        <div id="main"></div>
    </div>
</div>
<div id="digital clock"></div>
            <!--div id="mydiv"
	        onclick="document.getElementById('mydiv').innerHTML='Clicked time: '+Date()">
            click here for current time</div-->
<script>
function displaytime(){
	document.getElementById('digital clock').innerHTML = "Current time:"+ new Date();
}
setInterval(displaytime,500)

</script>

    <!--form action= "echo.php" method= "POST">
      Your input: <input name= "data" onkeyup="console.log('You have pressed a key')" id="data">
      <input class="button round" type= "button" value="Ajax echo" onclick="getEcho()">
      <input class="button round" type= "button" value="jQuery Ajax GET" onclick="jQueryAjax()">
      <input class="button round" type= "button" value="jQuery Ajax POST" onclick="jQueryAjaxPost()">
</form--->
<div id="response"></div>
<h3>WATER INTAKE BY WEIGHT</h3>
<hr>
weight in kgs: <input name = "weight" id="weight" align="center">
<input class = "button round" type="button" value="Get your per day water intake" onclick = "getIntake()" >
</form>
<div id = "water_intake" class = "response"></div>
<h3>BMI & FITNESS</h3>
<hr>
Height in cms: <input name = "height1" id="height1" align="center"><br>
Weight in kgs: <input name = "weight1" id="weight1" align="center">
<input class = "button round" type="button" value="BMI & FITNESS INSTRUCTOR" onclick = "getBMI()" >
</form>
<div id = "bmi_fitness" class = "response"></div>
<script>
    var canvas = document.getElementById("analog-clock");
    var ctx = canvas.getContext("2d");
    var radius = canvas.height / 2;
    ctx.translate(radius, radius);
    radius = radius * 0.90;
    setInterval(drawClock, 1000);

    function drawClock(){
        drawFace(ctx, radius);
        drawNumbers(ctx, radius);
        drawTime(ctx, radius);
    }
    </script>

<script>
    function getEcho(){
        var input = document.getElementById("data").value;
        if(input.length==0){
            return
        }
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange=function(){
            if(this.readyState==4 && this.status ==200){
                console.log("Received data="+xhttp.responseText);
                document.getElementById("response").innerText = "Response from Server:"+xhttp.responseText;
            }

        }
        xhttp.open("GET","echo.php?data="+input, true);
        xhttp.send();
        document.getElementById("data").value="";
    }
    </script>
    <script>
        function jQueryAjax(){
            var input = $("#data").val();
            if (input.length==0) return;
            $.get("echo.php?data="+input, function(result){
                $("#response").html("Response from server:"+result);
            }
            );
            $("data").val("");
        }
        </script>
    <script>
        function jQueryAjaxPost(){
            var input = $("#data").val();
            if (input.length==0) return;
            $.post("echo.php",
            {data : input } ,
            function(result){
                $("#response").html("Response from server:"+result);
            }
            );
            $("data").val("");
        }
        </script>
    <script>
        function getIntake(){
            var input = $("#weight").val();
            if (input.length==0) return;
            $.get("https://cca-vivekpravin-cloud.azurewebsites.net/water_intake?weight="+input,
            function(result){
            $("#water_intake").html("Response from server:"+result);}
            );
            $("#weight").val("");
        }
        </script>
    <script>
        function getBMI(){
            var weight = $("#weight1").val();
            var height = $("#height1").val();
            if (weight.length==0 || height.length == 0) return;
            console.log("the height is"+height)
            console.log("the weight is"+weight)
            $.get("https://cca-cloud-microservices1.herokuapp.com/bmi_calc?height="+height+"&weight="+weight,
            function(result){
            $("#bmi_fitness").html("Response from server:"+result);}
            );
            console.log("after response")
            $("#weight").val("");
            $("#height").val("");
        }
        </script>
</body>
</html>
